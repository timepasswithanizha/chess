const NoneAll = () => {
  document.getElementById("LOADER").style.display = "none";
  document.getElementById("NOTLOADING").style.display = "none";
  document.getElementById("CHESSORDER").style.display = "none";
  document.getElementById("CHESSCONTAINER").style.display = "none";
  document.getElementById("NOTFOUND").style.display = "none";
};

const defaultLoader = () => {
  NoneAll();
  document.getElementById("NOTLOADING").style.display = "block";
};

defaultLoader();

const startLoading = () => {
  NoneAll();
  document.getElementById("LOADER").style.display = "block";
};
const loadTable = (isOrder) => {
  NoneAll();
  isOrder === true ? document.getElementById("CHESSORDER").style.display = "block" : document.getElementById("CHESSCONTAINER").style.display = "block";
};

const searchNotFound = (isOrder) => {
  NoneAll();
  document.getElementById("NOTFOUND").style.display = "block";
  document.getElementById("404Search").value = isOrder ? "No order found" : "No container found";
};

const order = (search, response) => {
  document.getElementById('orderId').innerText = search;
  document.getElementById('order_amount').innerText = response.data.amount ? response.data.amount : "Not available";
  document.getElementById('order_chemicalID').innerText = response.data.chemicalID ? response.data.chemicalID : "Not available";
  document.getElementById('order_companyID').innerText = response.data.companyID ? response.data.companyID : "Not available";
  document.getElementById('order_contact').innerText = response.data.contact ? response.data.contact : "Not available";
  document.getElementById('order_containers').innerText = response.data.containers ? response.data.containers : "Not available";
  document.getElementById('order_dateEnd').innerText = response.data.dateEnd ? response.data.dateEnd : "Not available";
  document.getElementById('order_dateStart').innerText = response.data.dateStart ? response.data.dateStart : "Not available";
  document.getElementById('order_size').innerText = response.data.size ? response.data.size : "Not available";
  document.getElementById('order_suggestion_time').innerText = response.data.suggestion_time ? response.data.suggestion_time : "Not available";
  document.getElementById('order_totalamount').innerText = response.data.totalamount ? response.data.totalamount : "Not available";
  document.getElementById('order_totalamountunit').innerText = response.data.totalamountunit ? response.data.totalamountunit : "Not available";
  document.getElementById('order_type').innerText = response.data._type ? response.data._type : "Not available";
};

const container = (search, response) => {
  document.getElementById('containerId').innerText = search;
  document.getElementById('container_blockCell').innerText = response.data.blockCell ? response.data.blockCell : "Not available";
  document.getElementById('container_blockX').innerText = response.data.blockX ? response.data.blockX : "Not available";
  document.getElementById('container_blockY').innerText = response.data.blockY ? response.data.blockY : "Not available";
  document.getElementById('container_cargoContact').innerText = response.data.cargoContact ? response.data.cargoContact : "Not available";
  document.getElementById('container_cargoDate').innerText = response.data.cargoDate ? response.data.cargoDate : "Not available";
  document.getElementById('container_containerID').innerText = response.data.containerID ? response.data.containerID : "Not available";
  document.getElementById('container_containerID_explanation').innerText = response.data.containerID_explanation ? response.data.containerID_explanation : "Not available";
  document.getElementById('container_dateEnter').innerText = response.data.dateEnter ? response.data.dateEnter : "Not available";
  document.getElementById('container_dateLeave').innerText = response.data.dateLeave ? response.data.dateLeave : "Not available";
  document.getElementById('container_orderID').innerText = response.data.orderID ? response.data.orderID : "Not available";
  document.getElementById('container_size').innerText = response.data.size ? response.data.size : "Not available";
  document.getElementById('container_status').innerText = response.data.status ? response.data.status : "Not available";
  document.getElementById('container_trackerID').innerText = response.data.trackerID ? response.data.trackerID : "Not available";
  document.getElementById('container_volume').innerText = response.data.volume ? response.data.volume : "Not available";
  document.getElementById('container_weight').innerText = response.data.weight ? response.data.weight : "Not available";
  document.getElementById('container__type').innerText = response.data._type ? response.data._type : "Not available";
  document.getElementById('container_reservationID').innerText = response.data.reservationID ? response.data.reservationID : "Not available";
  document.getElementById('calculated_co2').innerText = response.data.calculatedCo2 ? response.data.calculatedCo2 : "Not available";
};

const searchOrder = async (event) => {
  event.preventDefault();
  const search = document.getElementById('search').value;
  if (search) {
    try {
      startLoading();
      const response = await axios.get('http://localhost:45735/searchOrder', { params: { search } });
      order(search, response);
      loadTable(true);
    } catch (error) {
      searchNotFound(true);
    }
  } else {
    defaultLoader();
  }
  document.getElementById('search').value = '';
};

const searchContainer = async (event) => {
  event.preventDefault();
  const search = document.getElementById('search').value;
  if (search) {
    try {
      startLoading();
      const response = await axios.get('http://localhost:45735/searchContainer', { params: { search } });
      container(search, response);
      loadTable(false);
    } catch (error) {
      searchNotFound(false);
    }
  } else {
    defaultLoader();
  }
  document.getElementById('search').value = '';
};


