'use strict';

var mongoose = require('mongoose'),
  Field = mongoose.model('Field');
  var fs = require('fs');
  var environmentJson = fs.readFileSync("./encryption/environment.json");
  var environment = JSON.parse(environmentJson);

    exports.list_fields = function(req, res) {
      Field.find({}, function(err, fields) {
        if (err)
          res.send(err);
        res.json(fields);
      });
    };

    exports.get_field = function(req, res) {
      Field.find({yk:req.body.yk}, function(err, fields) {
        if (err)
          res.send(err);
        res.json(fields);
      });
    };

    exports.create_a_field = function(req, res) {
      var new_field = new Field(req.body);
      new_field.save(function(err, fields) {
        if (err)
          res.send(err);

        res.json(fields);
      });
    };

    exports.update_a_field = function(req, res) {
      Field.findOneAndUpdate({_id: req.body.fieldID}, req.body, {new: true}, function(err, fields) {
        if (err)
          res.send(err);

        res.json(fields);
      });
    };
    exports.free_a_field = function(req, res) {
      Field.findOneAndUpdate({_id: req.body.fieldID}, {status:0}, {new: true}, function(err, fields) {
        if (err)
          res.send(err);

        res.json(fields);
      });
    };
    exports.reserve_field = function(req, res) {
      var con = req.body.container;

      if (con.length > 0)
      {
        for (var i = 0;i < con.length;i++)
        {
          Field.findOneAndUpdate({blockX: con[i].x,blockY: con[i].y, blockCell: con[i].cell}, {status:1}, {new: true}, function(err, fields) {
            if (err)
              console.log(err);
            else
            {
              console.log(fields);
            }
              //res.send(err);
          });

        }
        res.json({"type":"ok"});

      }
      else {
        res.json({"type":"error"});
      }

    };
    exports.delete_a_field = function(req, res) {
        Field.deleteOne({_id: req.body.fieldID}, function(err, fields) {
          if (err)
            res.send(err);
          res.json({ message: 'Field successfully deleted' });
        });

    };
