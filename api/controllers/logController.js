'use strict';

var mongoose = require('mongoose'),
  Log = mongoose.model('Log');
  var fs = require('fs');
  var environmentJson = fs.readFileSync("./encryption/environment.json");
  var environment = JSON.parse(environmentJson);

    exports.list_logs = function(req, res) {
      Log.find({}, function(err, logs) {
        if (err)
          res.send(err);
        res.json(logs);
      });
    };

    exports.list_logs_by_company = function(req, res) {
      Log.find({company:req.body.company}, function(err, logs) {
        if (err)
          res.send(err);
        res.json(logs);
      });
    };
    exports.list_logs_by_company_time = function(req, res) {
      Log.find({timestamp:{$gte:req.body.start,$lte:req.body.stop} }, function(err, logs) {
        if (err)
          res.send(err);
        
        res.json(logs);
      });
    };

    exports.get_logs = function(req, res) {
      Log.find({orderID:req.body.orderID}, function(err, logs) {
        if (err)
          res.send(err);
        res.json(logs);
      });
    };

    exports.create_a_log = function(req, res) {
      var new_log = new Log(req.body);
      new_log.save(function(err, logs) {
        if (err)
          res.send(err);

        res.json(logs);
      });
    };

    exports.update_a_log = function(req, res) {
      Log.findOneAndUpdate({_id: req.body.logID}, req.body, {new: true}, function(err, logs) {
        if (err)
          res.send(err);

        res.json(logs);
      });
    };
    exports.update_a_log_reported = function(req, res) {
      Log.findOneAndUpdate({_id: req.body.logID}, {reported:req.body.reportID}, {new: true}, function(err, logs) {
        if (err)
          res.send(err);

        res.json(logs);
      });
    };
    exports.update_a_kiho = function(req, res) {

      Log.findOneAndUpdate({_id: req.body.logID}, {reported:req.body.reportID}, {new: true}, function(err, logs) {
        if (err)
          res.send(err);

        res.json(logs);
      });
    };
