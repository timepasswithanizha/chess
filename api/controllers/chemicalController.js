'use strict';

var mongoose = require('mongoose'),
  Chemical = mongoose.model('Chemical');
  var fs = require('fs');
  var environmentJson = fs.readFileSync("./encryption/environment.json");
  var environment = JSON.parse(environmentJson);



    exports.list_chemicals = function(req, res) {
      Chemical.find({}, function(err, chemi) {
        if (err)
          res.send(err);
        res.json(chemi);
      });
    };

    exports.get_chemical = function(req, res) {
      Chemical.find({yk:req.body.yk}, function(err, chemi) {
        if (err)
          res.send(err);

        res.json(chemi);
      });
    };

    exports.create_a_chemical = function(req, res) {
      var new_chemi = new Chemical(req.body);
      new_chemi.save(function(err, chemi) {
        if (err)
        {
          res.send(err);
        }

        res.json(chemi);
      });
    };

    exports.update_a_chemical = function(req, res) {
      Chemical.findOneAndUpdate({_id: req.body.id}, req.body, {new: true}, function(err, chemi) {
        if (err)
          res.send(err);

        res.json(chemi);
      });
    };
    exports.delete_a_chemical = function(req, res) {
        Chemical.deleteOne({_id: req.body.chemicalID}, function(err, chemi) {
          if (err)
            res.send(err);
          res.json({ message: 'Chemical successfully deleted', id: req.body.chemicalID });
        });

    };
