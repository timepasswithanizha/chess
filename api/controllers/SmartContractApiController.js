'use strict';
const { CHESS, publicKey } = require('../../web3');

exports.home = function (req, res) {
  res.render('home');
};

exports.searchOrder = async (req, res) => {
  const { search } = req.query;
  try {
    const OrderData = await CHESS.methods.orders(search).call({ from: publicKey });
    if (OrderData.chemicalID === ""
      && OrderData.companyID === ""
      && OrderData.contact === ""
      && OrderData.containers === "0"
      && OrderData.dateEnd === "0"
      && OrderData.dateStart === "0"
      && OrderData.size === "0"
      && OrderData.suggestion_time === ""
      && OrderData.totalamount === "0"
      && OrderData.totalamountunit === "0"
      && OrderData._type === "0") {
      throw new Error('Not Found');
    }
    res.json(OrderData);
  } catch (error) {
    res.status(400).send({
      message: 'This is an error!'
    });
  }
};

exports.searchContainer = async (req, res) => {
  const { search } = req.query;
  try {
    const Container = await CHESS.methods.container(search).call({ from: publicKey });
    const ContainerData = await CHESS.methods.containerData(search).call({ from: publicKey });

    if (Container.blockX === "0" &&
      Container.blockY === "0" &&
      Container.dateEnter === "0" &&
      Container.dateLeave === "0" &&
      Container.size === "0" &&
      Container.status === "0" &&
      Container.volume === "0" &&
      Container.weight === "0" &&
      Container._type === "0") {
      throw new Error('Not Found');
    }

    res.json({ ...Container, ...ContainerData });
  } catch (error) {
    res.status(400).send({
      message: 'This is an error!'
    });
  }
};

