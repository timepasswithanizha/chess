'use strict';

var mongoose = require('mongoose'),
  GasJob = mongoose.model('GasJob'),
  GasCont = mongoose.model('GasCont');
  var fs = require('fs');
  var environmentJson = fs.readFileSync("./encryption/environment.json");
  var environment = JSON.parse(environmentJson);
  const async = require('async');

  exports.list_all_cont = function(req, res) {
    GasCont.find({}, function(err, cont) {
      if (err)
        res.send(err);
      res.json(cont);
    });
  };
  exports.create_a_cont = function(req, res) {
    var new_gas_cont = new GasCont(req.body);
    new_gas_cont.save(function(err, cont) {
      if (err)
      {
        res.send(err);
      }
      res.json(cont);
    });
  };
  exports.list_invent = function(req, res) {
    GasCont.find({$or:[{status:0},{status:1}]}, function(err, cont) {
      if (err)
        res.send(err);
      getServices(cont,function(err,r)
      {
        res.json(r);
      });
    });
  };
  exports.list_billing = function(req, res) {
    GasCont.find({$or:[{status:1},{status:2}]}, function(err, cont) {
      if (err)
        res.send(err);
      res.json(cont);
    });
  };
  exports.update_status_receive = function(req, res) {
    GasCont.findOneAndUpdate({_id: req.body.id}, {status:req.body.status,arrived:Date.now()}, {new: true}, function(err, cont) {
      if (err)
        res.send(err);

      var query = {'containerID':req.body.id};

      GasJob.findOne(query, function(err, serv) {
        if (err){
        }

        var j = serv.job;
        for (var i = 0; i < j.length; i++)
        {
          if (j[i].code < 5)
          {
            GasJob.findOneAndUpdate({_id:serv._id, "job._id":j[i]._id},{"$set":{"job.$.status":1,"job.$.timestamp":Date.now()}}, {new: true}, function(err, jk) {

            });
          }
        }
      });
      res.json(cont);
    });
  };
  exports.update_status_dispatch = function(req, res) {
    GasCont.findOneAndUpdate({_id: req.body.id}, {status:req.body.status,departed:Date.now()}, {new: true}, function(err, cont) {
      if (err)
        res.send(err);

        var query = {'containerID':req.body.id};

        GasJob.findOne(query, function(err, serv) {
          if (err){
          }

          var j = serv.job;
          for (var i = 0; i < j.length; i++)
          {
            if (j[i].code > 4 && j[i].code < 10)
            {
              GasJob.findOneAndUpdate({_id:serv._id, "job._id":j[i]._id},{"$set":{"job.$.status":1,"job.$.timestamp":Date.now()}}, {new: true}, function(err, jk) {
                
              });
            }
          }
        });
      res.json(cont);
    });
  };

  function getServices(containers, callback)
  {
  	var containery = [];
  	var iteratorFcn = function(container,done)
  	{

  		if (container._id == null)
  		{
  			done();
  			return;
  		}

  			var query = {'containerID':container._id};

        GasJob.findOne(query, function(err, serv) {
          if (err){
          }
          var c = {"container":container,"services":{}};
          if (serv != null)
          {
            c.services = serv;
          }
          containery.push(c);

    			done();
    			return;
        });
  	};
  	var doneIteratingFcn = function(err)
  	{
  		callback(err,containery);
  	};

  	async.forEach(containers, iteratorFcn, doneIteratingFcn);
  }
