'use strict';

var mongoose = require('mongoose'),
  Job = mongoose.model('Job');
  var fs = require('fs');
  var environmentJson = fs.readFileSync("./encryption/environment.json");
  var environment = JSON.parse(environmentJson);

  exports.create = function(req, res) {
    var new_job = new Job(req.body);
    new_job.save(function(err, jobi) {
      if (err)
        res.send(err);

      res.json(jobi);
    });
  };
  exports.list_jobs = function(req, res) {
    Job.find({}, function(err, jobs) {
      if (err)
        res.send(err);

      res.json(jobs);
    });
  };
  exports.container_jobs = function(req, res) {
    Job.find({containerID:req.body.containerID}, function(err, jobs) {
      if (err)
        res.send(err);

      res.json(jobs);
    });
  };
  exports.list_company = function(req, res) {
    Job.find({"company":req.body.company}, function(err, jobs) {
      if (err)
        res.send(err);

      res.json(jobs);
    });
  };
  exports.update = function(req, res) {
    //console.log(req.body);
    Job.findOneAndUpdate({orderID: req.body.orderID}, req.body, {upsert:true,setDefaultsOnInsert:true,new: true}, function(err, containers) {
      if (err)
        res.send(err);
      //console.log(containers);
      res.json(containers);
    });
  };


  exports.updatestatus = function(req, res) {

    Job.findOneAndUpdate({_id:req.body.jobID, "job._id":req.body.single_job},{"$set":{"job.$.status":1,"job.$.timestamp":req.body.timestamp}}, {new: true}, function(err, containers) {
      if (err)
        res.send(err);
      
      res.json(containers);
    });
  };
