'use strict';

var mongoose = require('mongoose'),
  Gas = mongoose.model('Gas');
  var fs = require('fs');
  var environmentJson = fs.readFileSync("./encryption/environment.json");
  var environment = JSON.parse(environmentJson);


  exports.list_all_gasses = function(req, res) {
    Gas.find({}, function(err, gasses) {
      if (err)
        res.send(err);
      res.json(gasses);
    });
  };
  exports.create_a_gas = function(req, res) {
    var new_gas = new Gas(req.body);
    new_gas.save(function(err, gasses) {
      if (err)
      {
        res.send(err);
      }
      res.json(gasses);
    });
  };
  exports.remove_a_gas = function(req, res) {
      Gas.deleteOne({_id: req.body.gasID}, function(err, gasses) {
        if (err)
          res.send(err);
        res.json({ message: 'Gas successfully deleted', id: req.body.gasID });
      });

  };
