'use strict';
const { CHESS, publicKey } = require('../../web3');
var mongoose = require('mongoose'),
  Order = mongoose.model('Order'),
  Container = mongoose.model('Container');
var fs = require('fs');
var environmentJson = fs.readFileSync("./encryption/environment.json");
var environment = JSON.parse(environmentJson);
const async = require('async');

exports.list_orders = function (req, res) {
  Order.find({}, function (err, orders) {
    if (err)
      res.send(err);
    res.json(orders);
  });
};
exports.get_order_by_company = function (req, res) {
  Order.find({ company: req.body.company }, function (err, orders) {
    if (err)
      res.send(err);
    getContainersForOrder(orders, function (err, r) {
      res.json(r);
    });

  });
};
exports.get_order = function (req, res) {
  Order.find({ orderID: req.body.orderID }, function (err, orders) {
    if (err)
      res.send(err);
    res.json(orders);
  });
};

exports.create_a_order = async (req, res) => {
  getMaxOrderNum(async (num) => {
    var n = 0;
    if (typeof num != "undefined" && num != null && num.length != null && num.length > 0) {
      n = num[0].orderID;
    }
    else {
      n = 0;
    }
    // req.body.orderID = n + 1;
    //req.body.containers = JSON.parse(req.body.containers);
    var new_order = new Order(req.body);
    await new_order.save();
    console.log([
      new_order.orderID.toString(),
      [
        new_order.company,
        new_order.contact,
        new_order.chemicalID,
        new_order.suggestion_time
      ],
      [
        new_order.amount,
        new_order.containers[0],
        new_order.type,
        new_order.size,
        new_order.dateStart,
        new_order.dateEnd,
        new_order.totalamount,
        new_order.totalamountunit,
      ]
    ]);
    const response = await CHESS.methods.writeOrder(
      new_order.orderID.toString(),
      [
        new_order.company,
        new_order.contact,
        new_order.chemicalID,
        new_order.suggestion_time
      ],
      [
        new_order.amount,
        new_order.containers[0],
        new_order.type,
        new_order.size,
        new_order.dateStart,
        new_order.dateEnd,
        new_order.totalamount,
        new_order.totalamountunit,
      ]
    ).send({ from: publicKey, gas: 5000000 });

    res.json({ ...response, new_order });
  });
};

exports.update_a_order = function (req, res) {
  Order.findOneAndUpdate({ orderID: req.body.orderID }, req.body, { new: true }, function (err, orders) {
    if (err)
      res.send(err);

    res.json(orders);
  });
};
exports.delete_a_order = async (req, res) => {
  await Order.deleteOne({ orderID: req.body.orderID });
  const response = await CHESS.methods.deleteOrder(
    req.body.orderID.toString()
  ).send({ from: publicKey, gas: 5000000 });
  res.json({ ...response, message: 'Order successfully deleted' });
};
var getMaxOrderNum = function (callback) {
  Order.find({}, ['orderID'], { limit: 1, sort: { orderID: -1 } }, function (err, orders) {
    if (err)
      callback(null);
    callback(orders);
  });
};
function getContainersForOrder(orders, callback) {
  var ordery = [];
  var iteratorFcn = function (order, done) {
    //console.log(container);
    if (order._id == null) {
      done();
      return;
    }
    getContainers(order.containers, function (err, con) {
      //Order.findOne({'orderID':parseInt(container.orderID)}, function(err, orders) {
      console.log(`getMaxOrderNum ~ error`, error);
      console.log(`getMaxOrderNum ~ error`, error);
      if (err) {
      }
      if (con != undefined && con != null) {
        var c = { "containers": [], "order": {} };
        c.order = order;
        c.containers = con;
        ordery.push(c);
      }
      done();
      return;
    });
  };
  var doneIteratingFcn = function (err) {
    callback(err, ordery);
  };

  async.forEach(orders, iteratorFcn, doneIteratingFcn);
}

function getContainers(containers, callback) {
  var containery = [];
  var iteratorFcn = function (container, done) {
    //console.log(container);
    if (container == null) {
      done();
      return;
    }
    Container.findOne({ 'reservationID': parseInt(container) }, function (err, con) {
      if (err) {
      }
      if (con != undefined && con != null) {
        containery.push(con);
      }
      done();
      return;
    });
  };
  var doneIteratingFcn = function (err) {
    callback(err, containery);
  };

  async.forEach(containers, iteratorFcn, doneIteratingFcn);
}
