'use strict';

var mongoose = require('mongoose'),
  Inspection = mongoose.model('Inspection');
  var fs = require('fs');
  var environmentJson = fs.readFileSync("./encryption/environment.json");
  var environment = JSON.parse(environmentJson);
  const async = require('async');

  exports.list = function(req, res) {
    Inspection.find({}, function(err, inspe) {
      if (err)
        res.send(err);
      res.json(inspe);
    });
  };
  exports.create = function(req, res) {
    var new_inspection = new Inspection(req.body);

    new_inspection.save(function(err, ins) {
      if (err)
        res.send(err);

      res.json(ins);
    });
  };
