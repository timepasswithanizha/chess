'use strict';

var mongoose = require('mongoose'),
  User = mongoose.model('User');
  var fs = require('fs');
  var environmentJson = fs.readFileSync("./encryption/environment.json");
  var environment = JSON.parse(environmentJson);


  exports.list_all_users = function(req, res) {
    User.find({}, function(err, users) {
      if (err)
        res.send(err);
      var u = [];
      if (users != undefined && users != null)
      {
        for (var i = 0;i< users.length;i++)
        {
          var us = users[i].toObject();
  				delete us.password;
          u.push(us);
        }

      }
      res.json(u);
    });
  };

  exports.list_company_users = function(req, res) {
    User.find({company:req.body.company}, function(err, users) {
      if (err)
        res.send(err);
      var u = [];
      if (users != undefined && users != null)
      {
        for (var i = 0;i< users.length;i++)
        {
          var us = users[i].toObject();
    			delete us.password;
          u.push(us);
        }
      }
      res.json(u);
    });
  };

  exports.create_a_user = function(req, res) {
    var new_user = new User(req.body);
    new_user.save(function(err, users) {
      if (err)
      {
        res.send(err);
      }
      var u = [];
      if (users != undefined && users != null)
      {
        for (var i = 0;i< users.length;i++)
        {
          var us = users[i].toObject();
    			delete us.password;
          u.push(us);
        }
      }
      res.json(u);
    });
  };

  exports.update_a_user = function(req, res) {
    User.findOneAndUpdate({_id: req.body.userID}, req.body, {new: true}, function(err, users) {
      if (err)
        res.send(err);
      var u = [];
      if (users != undefined && users != null)
      {
        for (var i = 0;i< users.length;i++)
        {
          var us = users[i].toObject();
      		delete us.password;
          u.push(us);
        }
      }
      res.json(u);
    });
  };
  exports.delete_a_user = function(req, res) {
      User.deleteOne({_id: req.body.userID}, function(err, users) {
        if (err)
          res.send(err);
        res.json({ message: 'User successfully deleted' });
      });

  };
