'use strict';

var mongoose = require('mongoose'),
  CompanyChemical = mongoose.model('CompanyChemical');
var fs = require('fs');
var environmentJson = fs.readFileSync("./encryption/environment.json");
var environment = JSON.parse(environmentJson);

exports.list_compchems = function (req, res) {
  CompanyChemical.find({}, function (err, chems) {
    if (err)
      res.send(err);
    res.json(fields);
  });
};

exports.get_compchem = function (req, res) {
  CompanyChemical.find({ yk: req.body.yk }, function (err, compchems) {
    if (err)
      res.send(err);
    res.json(compchems);
  });
};

exports.create_compchems = function (req, res) {
  var new_compchems = new CompanyChemical(req.body);
  new_compchems.save(function (err, compchems) {
    if (err)
      res.send(err);

    res.json(compchems);
  });
};

exports.update_compchems = function (req, res) {
  CompanyChemical.findOneAndUpdate({ _id: req.body.fieldID }, req.body, { new: true }, function (err, compchems) {
    if (err)
      res.send(err);

    res.json(compchems);
  });
};
exports.delete_compchems = function (req, res) {
  CompanyChemical.deleteOne({ _id: req.body.fieldID }, function (err, compchems) {
    if (err)
      res.send(err);
    res.json({ message: 'Field successfully deleted' });
  });

};

