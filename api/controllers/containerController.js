'use strict';
const { CHESS, publicKey } = require('../../web3');
var mongoose = require('mongoose'),
  Container = mongoose.model('Container'),
  Order = mongoose.model('Order'),
  Chemical = mongoose.model('Chemical'),
  Field = mongoose.model('Field'),
  Log = mongoose.model('Log'),
  Job = mongoose.model('Job');
var fs = require('fs');
var environmentJson = fs.readFileSync("./encryption/environment.json");
var environment = JSON.parse(environmentJson);
const async = require('async');

exports.list_containers = function (req, res) {
  Container.find({}, function (err, containers) {
    if (err)
      res.send(err);

    res.json(containers);
  });
};

exports.get_container_by_container_id = function (req, res) {
  Container.find({ containerID: req.body.containerID }, function (err, containers) {
    if (err)
      res.send(err);
    res.json(containers);
  });
};

exports.list_containers_by_company = function (req, res) {
  Container.find({}, function (err, containers) {
    if (err)
      res.send(err);

    getContainersForCompany(containers, req.body.companyID, function (err, co) {
      getMaxContainerNum(function (r) {
        var j = { "containers": co, "max": r };
        res.json(j);
      });

    });

  });
};
var getMaxContainerNum = function (callback) {
  Container.find({}, ['reservationID'], { limit: 1, sort: { reservationID: -1 } }, function (err, containers) {
    if (err)
      callback(null);
    if (containers != undefined && containers != null && containers.length > 0) {
      callback(containers[0].reservationID);
    }
    else {
      callback(1000);
    }
  });
};
exports.get_container = function (req, res) {
  Container.find({ reservationID: req.body.reservationID }, function (err, containers) {
    if (err)
      res.send(err);
    res.json(containers);
  });
};

exports.get_one_container = function (req, res) {
  Container.find({ reservationID: req.body.reservationID }, function (err, containers) {
    if (err)
      res.send(err);
    getContainerOrder(containers, function (err, r) {
      getContainerChemical(r, function (err2, rr) {
        getContainerField(rr, function (err3, rrr) {
          res.json(rrr);
        });
      });
    });
  });
};

exports.get_inventory = function (req, res) {
  Container.find({ $and: [{ status: { $gt: 0 } }, { status: { $lt: 4 } }] }, function (err, containers) {
    if (err)
      res.send(err);
    getContainerOrder(containers, function (err, r) {
      getContainerChemical(r, function (err2, rr) {
        getContainerField(rr, function (err3, rrr) {
          res.json(rrr);
        });
      });
    });
  });
};

exports.get_containers_by_status = function (req, res) {
  if (req.body.time == 'all') {
    Container.find({ status: req.body.status }, function (err, containers) {
      if (err)
        res.send(err);
      getContainerOrder(containers, function (err, r) {
        getContainerChemical(r, function (err2, rr) {
          getContainerField(rr, function (err3, rrr) {
            getServices(rrr, function (err4, rrrr) {
              //console.log(rrrr);
              res.json(rrrr);
            });

          });
        });
      });
    });
  }
  else {
    Container.find({ status: req.body.status }, function (err, containers) {
      if (err)
        res.send(err);
      getContainerOrderTime(containers, req.body, function (err, r) {
        getContainerChemical(r, function (err2, rr) {
          getContainerField(rr, function (err3, rrr) {
            getServices(rrr, function (err4, rrrr) {
              res.json(rrrr);
            });
          });
        });
      });
    });
  }
};

exports.create_a_container = async (req, res) => {
  var new_container = new Container(req.body);
  new_container.type = parseInt(req.body.type);
  if (req.body.type == 0) {
    new_container.weight = 32500;
    new_container.volume = 24;
  }
  else if (req.body.type == 1) {
    if (req.body.size == 1) {
      new_container.weight = 32500;
      new_container.volume = 32;
    }
    else if (req.body.size == 2) {
      new_container.weight = 32500;
      new_container.volume = 65;
    }

  }
  else {
    new_container.weight = 1000;
    new_container.volume = 1;
  }
  await new_container.save();
  const response = await CHESS.methods.writeContainer(
    new_container.containerID.toString(),
    [
      new_container.containerID_explanation,
      new_container.reservationID,
      new_container.blockCell,
      new_container.containerID,
      new_container.orderID,
      new_container.trackerID,
      new_container.cargoDate,
      new_container.cargoContact
    ],
    [
      new_container.dateEnter,
      new_container.dateLeave,
      new_container.weight,
      new_container.volume,
      new_container.type,
      new_container.size,
      new_container.blockX,
      new_container.blockY,
      new_container.status,
    ]
  ).send({ from: publicKey, gas: 5000000 });
  res.json({ ...response, new_container });
};

exports.reserve_a_container = function (req, res) {
  var con = req.body.container;
  if (con.length > 0) {
    for (var i = 0; i < con.length; i++) {
      var we = 1000;
      var vol = 1;
      if (req.body.type == 0) {
        we = 32500;
        vol = 24;
      }
      else if (req.body.type == 1) {
        if (req.body.size == 1) {
          we = 32500;
          vol = 32;
        }
        else if (req.body.size == 2) {
          we = 32500;
          vol = 65;
        }
      }
      else {
        we = 1000;
        vol = 1;
      }
      var c = {
        "reservationID": con[i].ccode,
        "blockCell": con[i].cell,
        "blockX": con[i].x,
        "blockY": con[i].y,
        "containerID": con[i].containerid,
        "orderID": req.body.order.orderID,
        "status": 0,
        "trackerID": "",
        "type": req.body.type,
        "size": req.body.size,
        "weight": we,
        "volume": vol
      };
      var new_container = new Container(c);
      new_container.save(function (err, containers) {
        if (err)
          console.log("new container error");
        else {
          var o = {
            "orderID": req.body.order.orderID,
            "containerID": containers._id,
            "message": "container created with new order",
            "code": 0,
            "timestamp": Date.now(),
            "userID": req.body.userID,
            "company": req.body.company
          };
          var new_log = new Log(o);
          new_log.save(function (err, logs) {
          });
        }
      });
    }
    res.json({ "type": "ok" });
  }
  else {
    res.json({ "type": "error" });
  }
};

exports.update_a_container = function (req, res) {
  Container.findOneAndUpdate({ _id: req.body.containerID }, req.body, { new: true }, function (err, containers) {
    if (err)
      res.send(err);

    res.json(containers);
  });
};

exports.update_a_container_id = function (req, res) {
  Container.findOneAndUpdate({ reservationID: req.body.reservationID }, { containerID: req.body.containerID }, { new: true }, function (err, containers) {
    if (err)
      res.send(err);

    res.json(containers);
  });
};

exports.update_a_container_id2 = function (req, res) {
  Container.findOneAndUpdate({ reservationID: req.body.reservationID }, { containerID: req.body.containerID, containerID_explanation: req.body.explanation }, { new: true }, function (err, containers) {
    if (err)
      res.send(err);

    res.json(containers);
  });
};

exports.update_a_container_status = function (req, res) {
  if (req.body.status == 1) {
    Container.findOneAndUpdate({ reservationID: req.body.reservationID }, { status: req.body.status, trackerID: req.body.trackerID, dateEnter: req.body.dateEnter, containerID: req.body.containerID }, { new: true }, function (err, containers) {
      if (err)
        res.send(err);

      var o = {
        "orderID": containers.orderID,
        "containerID": containers._id,
        "message": "container arrived to field",
        "code": 1,
        "timestamp": Date.now(),
        "userID": req.body.userID,
        "company": req.body.company,
        "applatitude": req.body.latitude,
        "applongitude": req.body.longitude
      };
      var new_log = new Log(o);
      new_log.save(function (err, logs) {
        update_a_kiho(logs._id);
      });
      res.json(containers);
    });
  }
  else if (req.body.status == 4) {
    Container.findOneAndUpdate({ reservationID: req.body.reservationID }, { status: req.body.status, trackerID: req.body.trackerID, dateLeave: req.body.dateLeave }, { new: true }, function (err, containers) {
      if (err)
        res.send(err);

      var o = {
        "orderID": containers.orderID,
        "containerID": containers._id,
        "message": "container has left the field",
        "code": 5,
        "timestamp": Date.now(),
        "userID": req.body.userID,
        "company": req.body.company,
        "applatitude": req.body.latitude,
        "applongitude": req.body.longitude
      };
      var new_log = new Log(o);
      new_log.save(function (err, logs) {
        update_a_kiho(logs._id);
      });
      res.json(containers);
    });
  }
  else {
    Container.findOneAndUpdate({ reservationID: req.body.reservationID }, { status: req.body.status }, { new: true }, function (err, containers) {
      if (err)
        res.send(err);
      if (req.body.status == 2) {
        var o = {
          "orderID": containers.orderID,
          "containerID": containers._id,
          "message": "container moved to field",
          "code": 2,
          "timestamp": Date.now(),
          "userID": req.body.userID,
          "company": req.body.company,
          "applatitude": req.body.latitude,
          "applongitude": req.body.longitude
        };
        var new_log = new Log(o);
        new_log.save(function (err, logs) {
          update_a_kiho(logs._id);
        });
      }
      else if (req.body.status == 3) {
        var o = {};
        if (req.body.transit == '0') {
          Container.findOneAndUpdate({ reservationID: req.body.reservationID }, { cargoDate: req.body.cargoDate, cargoContact: req.body.cargoContact }, { new: true }, function (err, containers55) {
          });

          o = {
            "orderID": containers.orderID,
            "containerID": containers._id,
            "message": "container marked for removal",
            "code": 3,
            "timestamp": Date.now(),
            "userID": req.body.userID,
            "company": req.body.company,
            "applatitude": req.body.latitude,
            "applongitude": req.body.longitude
          };
        }
        else {
          o = {
            "orderID": containers.orderID,
            "containerID": containers._id,
            "message": "container moved from field",
            "code": 4,
            "timestamp": Date.now(),
            "userID": req.body.userID,
            "company": req.body.company,
            "applatitude": req.body.latitude,
            "applongitude": req.body.longitude
          };
        }
        var new_log = new Log(o);
        new_log.save(function (err, logs) {
          update_a_kiho(logs._id);
        });
      }
      res.json(containers);
    });
  }

};

exports.delete_a_container = async (req, res) => {
  await Container.deleteOne({ _id: req.body.containerID });
  const response = await CHESS.methods.deleteContainer(
    req.body.containerID.toString()
  ).send({ from: publicKey, gas: 5000000 });
  res.json({ ...response, message: 'Container successfully deleted' });

};

function update_a_kiho(id) {
  var kiho = require('./kihoController');
  kiho.updateKihoLocation("", function (rrrr) {

    if (rrrr.type == "ok") {
      Log.findOneAndUpdate({ _id: id }, { "kiholatitude": rrrr.msg.lat, "kiholongitude": rrrr.msg.lon }, { new: true }, function (err, logs) {
        //if (err)
        //res.send(err);

        //res.json(logs);
      });
    }
  });


};

function getContainerOrder(containers, callback) {
  var containery = [];
  var iteratorFcn = function (container, done) {

    if (container._id == null) {
      done();
      return;
    }

    var query = { 'orderID': parseInt(container.orderID) };

    Order.findOne(query, function (err, orders) {
      if (err) {
      }
      var c = { "container": container, "order": "" };
      c.order = orders;
      containery.push(c);

      done();
      return;
    });
  };
  var doneIteratingFcn = function (err) {
    callback(err, containery);
  };

  async.forEach(containers, iteratorFcn, doneIteratingFcn);
}



function getContainerOrderTime(containers, data, callback) {
  var containery = [];
  var iteratorFcn = function (container, done) {

    if (container._id == null) {
      done();
      return;
    }
    Order.findOne({ 'orderID': parseInt(container.orderID), 'dateStart': { $lte: data.dateStart }, 'dateEnd': { $gte: data.dateEnd } }, function (err, orders) {

      if (err) {
      }
      if (orders != undefined && orders != null) {
        var c = { "container": container, "order": "" };
        c.order = orders;
        containery.push(c);
      }
      done();
      return;
    });
  };
  var doneIteratingFcn = function (err) {
    callback(err, containery);
  };

  async.forEach(containers, iteratorFcn, doneIteratingFcn);
}

function getContainerChemical(containers, callback) {
  var containery = [];
  var iteratorFcn = function (container, done) {
    if (container.container == null) {
      done();
      return;
    }

    var query = { '_id': container.order.chemicalID };

    Chemical.findOne(query, function (err, chemi) {
      if (err) {
        //res.send(err);
        done();
        return;
      }
      var c = { "container": container.container, "order": container.order, "chemical": "" };
      c.chemical = chemi;
      containery.push(c);
      done();
      return;
    });
  };
  var doneIteratingFcn = function (err) {
    callback(err, containery);
  };

  async.forEach(containers, iteratorFcn, doneIteratingFcn);
}

function getContainerField(containers, callback) {
  var containery = [];
  var iteratorFcn = function (container, done) {
    if (container.container == null) {
      done();
      return;
    }
    var query = { 'blockCell': container.container.blockCell, 'blockX': container.container.blockX, 'blockY': container.container.blockY };

    Field.findOne(query, function (err, fields) {
      if (err) {
        done();
        return;
        //res.send(err);
      }
      var c = { "container": container.container, "order": container.order, "chemical": container.chemical, "field": "" };
      c.field = fields;
      containery.push(c);
      done();
      return;
    });
  };
  var doneIteratingFcn = function (err) {
    callback(err, containery);
  };

  async.forEach(containers, iteratorFcn, doneIteratingFcn);
}

function getServices(containers, callback) {
  var containery = [];
  var iteratorFcn = function (container, done) {
    if (container.container == null) {
      done();
      return;
    }

    var query = { "orderID": container.order._id };


    Job.findOne(query, function (err, jobs) {
      if (err) {

        done();
        return;

      }

      var c = { "container": container.container, "order": container.order, "chemical": container.chemical, "field": container.field, "services": "" };
      c.services = jobs;

      containery.push(c);
      done();
      return;
    });
  };
  var doneIteratingFcn = function (err) {
    callback(err, containery);
  };

  async.forEach(containers, iteratorFcn, doneIteratingFcn);
}


function getContainersForCompany(containers, companyID, callback) {
  var containery = [];
  var iteratorFcn = function (container, done) {

    if (container == null) {
      done();
      return;
    }
    Order.findOne({ orderID: container.orderID, company: companyID }, function (err, orders) {

      if (err) {
        //res.send(err);
        //done();
        //return;
      }
      if (orders != undefined && orders != null) {
        if (orders.company == companyID) {
          containery.push(container);
        }
      }
      done();
      return;
    });
  };
  var doneIteratingFcn = function (err) {
    callback(err, containery);
  };

  async.forEach(containers, iteratorFcn, doneIteratingFcn);
}
