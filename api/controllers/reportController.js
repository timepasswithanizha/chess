'use strict';

var mongoose = require('mongoose'),
  Report = mongoose.model('Report');
  var fs = require('fs');
  var environmentJson = fs.readFileSync("./encryption/environment.json");
  var environment = JSON.parse(environmentJson);


  exports.list_reports = function(req, res) {
    Report.find({}, function(err, reports) {
      if (err)
        res.send(err);
      res.json(reports);
    });
  };
  exports.list_reports_order = function(req, res) {
    Report.find({orderID:req.body.orderID}, function(err, reports) {
      if (err)
        res.send(err);
      res.json(reports);
    });
  };
  exports.create = function(req, res) {
    console.log(req.body);
    var new_report = new Report(req.body);

    new_report.save(function(err, re) {
      console.log(err,re);
      if (err)
        res.send(err);

      res.json(re);
    });
  };
  exports.removepart = function(req, res) {
    Report.find({orderID:req.body.orderID}, function(err, reports) {
      if (err)
        res.send(err);
      res.json(reports);
    });
  };
