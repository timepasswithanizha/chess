'use strict';
const { CHESS, publicKey } = require('../../web3');
var mongoose = require('mongoose'),
  Container = mongoose.model('Container'),
  GasJob = mongoose.model('GasJob');
var fs = require('fs');
var environmentJson = fs.readFileSync("./encryption/environment.json");
var environment = JSON.parse(environmentJson);
// job model job.code
//0 = container move less than 750m to chess field,
// 1 = container move more than 750m to chess field,
// 2 = container move from all weather terminal to chess field,
//3 = container move from somewhere else to chess field,
// 4 = container owner handles move to chess field(only lift at chess field)
// 5 = chess field to place closer than 750m, 
// 6 = chess field to place farther than 750m,
// 7 = chess field to all weather terminal,
// 8 = chess field to elsewhere,
// 9 = container owner handles transport from chess field (only lift),
// 10 = Solas VGM(container weighting on scale),
//11 = fork lift scale (container weighting on scale),
// 12 = harbour scale(container weighting on scale), 13 = port tower scale(container weighting on scale),
// 14 = removal of container warning stickers, 15 = adding container warning stickers

// if movement from/to elsewhere (codes 3 and 8)
// in job.msg
// 1 = Raahe, 2 = Oulu, 3 = Vaasa, 4 = Rauma, 5 = Turku, 6 = Helsinki, 7 = Kotka, 8 = Sotkamo
const calculatedCo2 = (caseNumber, weight) => {
  const tonnes = weight / 1000;
  switch (caseNumber) {
    case 1:
      return tonnes * 1560 * 62;
    case 2:
      return tonnes * 1993 * 62;
    case 3:
      return tonnes * 1206 * 62;
    case 4:
      return tonnes * 3597 * 62;
    case 5:
      return tonnes * 4354 * 62;
    case 6:
      return tonnes * 4807 * 62;
    case 7:
      return tonnes * 4865 * 62;
    case 8:
      return tonnes * 2855 * 62;
  }
};
exports.list_all_jobs = function (req, res) {
  GasJob.find({}, function (err, job) {
    if (err)
      res.send(err);
    res.json(job);
  });
};
exports.create_a_job = async (req, res) => {
  const container = await Container.findOne({ containerID: req.body.containerID }).exec();
  const co2 = calculatedCo2(req.body.job[0].code, container.weight);
  const response = await CHESS.methods.writeCo2(req.body.containerID, co2).send({ from: publicKey, gas: 5000000 });
  var new_job = new GasJob(req.body);
  await new_job.save();
  res.json({ ...response, new_job });
};
exports.update = function (req, res) {
  //console.log(req.body);
  GasJob.findOneAndUpdate({ containerID: req.body.containerID }, req.body, { upsert: true, setDefaultsOnInsert: true, new: true }, function (err, jobs) {
    if (err)
      res.send(err);
    //console.log(containers);
    res.json(jobs);
  });
};
