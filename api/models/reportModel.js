'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReportSchema = new Schema ({
  orderID: {
    type: String,
    required:"missing orderID"
  },
  containerID: {
    type: String,
    required:"missing containerID"
  },
  company:{
    type:String,
    required: "missing company"
  },
  timestamp:
  {
    type: Number,
    required: "missing timestamp"
  },
  userID:
  {
    type: String
  },
  start:
  {
    type:Number,
    required: "missing start"
  },
  stop:
  {
    type:Number,
    required: "missing stop"
  },
  message:
  {
    type: String
  },
  logs:[ //mongo id array from log
    {
      type:String
    }
  ]


});
module.exports = mongoose.model('Report', ReportSchema);
