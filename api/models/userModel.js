'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema ({
  username:
  {
    type: String,
    required: 'No username',
    unique: true
  },
  company:
  {
    type: String,
    required: 'No company'
  },
  password:
  {
    type: String,
    required: 'No password'
  },
  userlevel:
  {
    type: String,
    required: 'No userlevel'
  },
  email:
  {
    type: String
  },
  phone:
  {
    type: String
  }

});

module.exports = mongoose.model('User', UserSchema);
