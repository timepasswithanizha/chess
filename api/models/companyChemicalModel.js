'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CompanyChemicalSchema = new Schema({
  company:
  {
    type: String,
    required: 'missing company'
  },
  chemicalID:
  {
    type: String,
    required: 'missing chemicalID'
  }

});


module.exports = mongoose.model('CompanyChemical', CompanyChemicalSchema);

