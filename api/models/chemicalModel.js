'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChemicalSchema = new Schema ({
  name:
  {
    type: String,
    required: 'no name'
  },
  additional:
  {
    type:String
  },
  company:
  {
    type: String,
    required: 'no company'
  },
  yk:
  {
    type: Number,
    required: 'no yk'
  },
  class:
  {
    type: Number,
    required: 'no class'
  },
  clp:[Number],
  notallowed:[Number],
  seveso:[Number]


});


module.exports = mongoose.model('Chemical', ChemicalSchema);
