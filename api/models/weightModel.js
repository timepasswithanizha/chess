'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WeightSchema = new Schema ({
  orderID: {
    type: String,
    required:"missing orderID"
  },
  containerID: {
    type: String,
    required:"missing containerID"
  },
  company:{
    type:String,
    required: "missing company"
  },

  code: {
    type:Number,
    required:"missing code"
  },
  timestamp:
  {
    type: Number,
    required: "missing timestamp"
  },
  userID:
  {
    type: String
  }
});
module.exports = mongoose.model('Weight', WeightSchema);
