'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var JobSchema = new Schema({
  orderID: { //mongodb order _id
    type: String,
    required: "missing orderID"
  },
  containerID: { //mongodb container _id
    type: String,
    required: "missing containerID"
  },
  company: {
    type: String,
    required: "missing company"
  },
  job: [
    {
      code: { //0 = siirto alle 750m->keva, 1 = siirto yli 750m->keva, 2 = siirto AWT:ta ->keva,
        //3 = siirto muualta ->keva, 4 = hoitaa itse nosto siirtopaikalta  siirtopaikka -> keva
        // 5 = keva->siirto alle 750, 6 = keva->siirto yli 750m, 7 = keva->siirto AWT, 8 = keva -> siirto muualle,
        // 9 = hoitaa itse nosto siirtopaikalla keva->siirtopaikka, 10 = Solas VGM punnistus,
        //11 = kurottajapunnitus, 12 = vaaka satama, 13 = vaaka port tower, 14 = imo tarrojen poisto, 15 = imo tarrojen lisäys
        type: Number,
      },
      status: { // 0 = not done, 1 = done
        type: Number
      },
      msg: //1 = Raahe, 2 = Oulu, 3 = Vaasa, 4 = Rauma, 5 = Turku, 6 = Helsinki, 7 = Kotka, 8 = Sotkamo
      {
        type: String
      },
      timestamp: {
        type: Number
      }
    }
  ],
  timestamp:
  {
    type: Number,
    required: "missing timestamp"
  },
  userID:
  {
    type: String
  }
});
module.exports = mongoose.model('Job', JobSchema);
