'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ContainerSchema = new Schema ({
  reservationID:
  {
    type: Number,
    required: 'missing reservationID',
    unique: true
  },
  blockCell:
  {
    type:Number,
    required: 'missing blockCell'
  },
  blockX:
  {
    type: Number,
    required: 'missing blockX'
  },
  blockY:
  {
    type: Number,
    required: 'missing blockY'
  },
  containerID:
  {
    type: String
  },
  orderID:
  {
    type: String,
    required: 'missing orderID'
  },
  status: //0 =  varattu, 1 = saapunut viedään paikalleen, 2 = paikalla, 3 = paikalla viedään pois, 4 = poistunut
  {
    type: Number,
    required: 'missing status'
  },
  trackerID:
  {
    type: String
  },
  dateEnter:
  {
    type: Number
  },
  dateLeave:
  {
    type: Number
  },
  weight:
  {
    type: Number
  },
  volume:
  {
    type: Number
  },
  type: //0 = säiliökontti, 1 = irtolastikontti, 2 = muu?
  {
    type:Number,
    required: 'missing container type'
  },
  size:
  {
    type: Number
  },
  containerID_explanation:
  {
    type: String
  },
  cargoDate: //date when leaving from site
  {
    type: String
  },
  cargoContact: //contact for leaving cargo
  {
    type: String
  }

});



module.exports = mongoose.model('Container', ContainerSchema);
