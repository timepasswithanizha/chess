'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GasSchema = new Schema ({

  name:
  {
    type: String,
    required: 'No name'
  },
  area:
  {
    type: String,
    required: 'No area'
  }

});

module.exports = mongoose.model('Gas', GasSchema);
