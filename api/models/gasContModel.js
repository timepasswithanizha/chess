'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GasContSchema = new Schema ({

  gas: //gas name
  {
    type: String,
    required: 'No name'
  },
  company: //company name
  {
    type: String,
    required: 'No area'
  },
  field: //field, 0 = R-1, 1 = R-2, 2 = R-3
  {
    type: Number,
    required: 'no field'
  },
  amount:
  {
    type:Number,
    required: 'no amount'
  },
  status: //0 = ei saapunut, 1= paikalla, 2 = poistunut, 3 = poistettu, 4 = arkistoitu
  {
    type:Number,
    required: 'no status'
  },
  arrived:
  {
    type: Number
  },
  departed:
  {
    type:Number
  }

});

module.exports = mongoose.model('GasCont', GasContSchema);
