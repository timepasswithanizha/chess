'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var InspectionSchema = new Schema ({

  company: //company id
  {
    type:String,
    required:'missing company'
  },
  containerID:{ //container reservationID
    type: String
  },
  mode:{
    type: String
  },
  checkpoint:[
    {
      name:{
        type: String
      },
      info:{
        type: String
      },
      status:{
        type: Number
      }
    }
  ],
  userID:
  {
    type: String
  }
});


module.exports = mongoose.model('Inspection', InspectionSchema);
