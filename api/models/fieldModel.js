'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FieldSchema = new Schema ({
  name: //paikan tunnus
  {
    type:String,
    required: 'missing name'
  },
  status: //0 = vapaa, 1 = varattu, 2 = pois käytöstä
  {
    type: Number,
    required: 'missing status'
  },
  location: //tulevaisuutta varten mikä sijainti (koko kentän tunnus)
  {
    type: String,
    required: 'missing location'
  },
  blockCell: //paikannumero alkaa nollasta
  {
    type: Number,
    required: 'missing blockCell'
  },
  blockX: //paikka X-akselilla varastokentällä vasen yläkulma 0
  {
    type:Number,
    required: 'missing blockX'
  },
  blockY: //paikka Y-akselilla varastokentällä vasen yläkulma 0
  {
    type:Number,
    required: 'missing blockY'
  },
  latitude:
  {
    type: String
  },
  longitude:
  {
    type: String
  }
});


module.exports = mongoose.model('Field', FieldSchema);
