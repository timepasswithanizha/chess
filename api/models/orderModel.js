'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrderSchema = new Schema ({
  orderID: //juokseva numero
  {
    type: Number,
    required: 'missing orderID'
  },
  company:
  {
    type:String,
    required:'missing company'
  },
  contact:
  {
    type:String,
    required:'missing contact'
  },
  amount:
  {
    type:Number,
    required: 'missing amount'
  },
  type: //0 = säiliökontti, 1 = irtolastikontti, 2 = muu?
  {
    type:Number,
    required: 'missing container type'
  },
  containers:[Number],
  dateEnd:
  {
    type: Number,
    required:'missing dateEnd'
  },
  dateStart:
  {
    type: Number,
    required: 'missing dataStart'
  },
  kttlink:
  {
    type: String
  },
  size: //0 = 1 TEU = 6m , 1 = 2 TEU = 12m
  {
    type: Number
  },
  totalamount:
  {
    type: Number
  },
  totalamountunit:
  {
    type: Number
  },
  yknumber:
  {
    type: String,
    required: 'missing yk'
  },
  chemicalID:
  {
    type: String,
    required: 'missing chemical id'
  },
  chemicalplus:
  {
    type: String
  },
  suggestion_time:
  {
    type: String
  }
});


module.exports = mongoose.model('Order', OrderSchema);
