'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LogSchema = new Schema ({
  orderID: {
    type: String,
    required:"missing orderID"
  },
  containerID: {
    type: String,
    required:"missing containerID"
  },
  company:{
    type:String,
    required: "missing company"
  },
  message: { // if code = 6
    type: String
  },
  code: { //0 = new order, 1 = container arrival, 2 = container in its place, 3 = container marked for leaving, 4 = container leaving its place ,5 = container departure, 6 = job
    type:Number,
    required:"missing code"
  },
  timestamp:
  {
    type: Number,
    required: "missing timestamp"
  },
  reported: //report mongo id
  {
    type: String
  },
  userID:
  {
    type: String
  },
  applatitude:{
      type:String
  },
  applongitude:{
      type:String
  },
  kiholatitude:{
      type:String
  },
  kiholongitude:{
      type:String
  }

});




module.exports = mongoose.model('Log', LogSchema);
