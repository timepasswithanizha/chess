'use strict';
module.exports = function(app)
{
  var containerRoutes = require('../controllers/containerController');

  app.route('/container/getall')
    .post(containerRoutes.list_containers);
  app.route('/container/get_containers_by_company')
    .post(containerRoutes.list_containers_by_company);
  app.route('/container/getcontainer')
    .post(containerRoutes.get_container);
  app.route('/container/getinventory')
    .post(containerRoutes.get_inventory);
  app.route('/container/create')
    .post(containerRoutes.create_a_container);
  app.route('/container/update')
    .post(containerRoutes.update_a_container);
  app.route('/container/remove')
    .post(containerRoutes.delete_a_container);
  app.route('/container/reserve')
    .post(containerRoutes.reserve_a_container);
  app.route('/container/getbystatus')
    .post(containerRoutes.get_containers_by_status);
  app.route('/container/updatestatus')
    .post(containerRoutes.update_a_container_status);
  app.route('/container/updatecontainerid')
    .post(containerRoutes.update_a_container_id);
  app.route('/container/updatecontainerid2')
    .post(containerRoutes.update_a_container_id2);
  app.route('/container/get_one_container')
    .post(containerRoutes.get_one_container);
  app.route('/container/getInfo')
    .post(containerRoutes.get_container_by_container_id);







};
