'use strict';
module.exports = function(app)
{
  var reportRoutes = require('../controllers/reportController');

  app.route('/report/getall')
    .post(reportRoutes.list_reports);
  app.route('/report/getbyorder')
    .post(reportRoutes.list_reports_order);
  app.route('/report/create')
    .post(reportRoutes.create);
  app.route('/report/removepart')
    .post(reportRoutes.removepart);

};
