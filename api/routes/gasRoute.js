'use strict';
module.exports = function(app)
{
  var gasRoutes = require('../controllers/gasController');

  app.route('/gas/getall')
    .post(gasRoutes.list_all_gasses);
  app.route('/gas/create')
    .post(gasRoutes.create_a_gas);
  app.route('/gas/remove')
    .post(gasRoutes.remove_a_gas);

};
