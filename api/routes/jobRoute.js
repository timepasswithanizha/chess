'use strict';
module.exports = function(app)
{
  var jobRoutes = require('../controllers/jobController');


  app.route('/job/create')
    .post(jobRoutes.create);
  app.route('/job/getall')
    .post(jobRoutes.list_jobs);
  app.route('/job/getcompany')
    .post(jobRoutes.list_company);
  app.route('/job/update')
    .post(jobRoutes.update);
  app.route('/job/containerjobs')
    .post(jobRoutes.container_jobs);
  app.route('/job/updatestatus')
    .post(jobRoutes.updatestatus);


};
