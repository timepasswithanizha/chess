'use strict';
module.exports = function(app)
{
  var gasContRoutes = require('../controllers/gasContController');

  app.route('/gascont/getall')
    .post(gasContRoutes.list_all_cont);
  app.route('/gascont/create')
    .post(gasContRoutes.create_a_cont);
  app.route('/gascont/getinventory')
    .post(gasContRoutes.list_invent);
  app.route('/gascont/getbilling')
    .post(gasContRoutes.list_billing);
  app.route('/gascont/changestatus_receive')
    .post(gasContRoutes.update_status_receive);
  app.route('/gascont/changestatus_dispatch')
    .post(gasContRoutes.update_status_dispatch);

};
