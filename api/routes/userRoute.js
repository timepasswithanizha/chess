'use strict';
module.exports = function(app)
{
  var userRoutes = require('../controllers/userController');

  app.route('/user/getall')
    .post(userRoutes.list_all_users);
  app.route('/user/getcompany')
    .post(userRoutes.list_company_users);
  app.route('/user/create')
    .post(userRoutes.create_a_user);
  app.route('/user/update')
    .post(userRoutes.update_a_user);
  app.route('/user/remove')
    .post(userRoutes.delete_a_user);
};
