'use strict';
module.exports = function(app)
{
  var inspectionRoutes = require('../controllers/inspectionController');

  app.route('/inspection/getall')
    .post(inspectionRoutes.list);
  app.route('/inspection/create')
    .post(inspectionRoutes.create);
  /*app.route('/inspection/update')
    .post(inspectionRoutes.update);
  app.route('/inspection/remove')
    .post(inspectionRoutes.delete);*/

};
