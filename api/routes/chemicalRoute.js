'use strict';
module.exports = function(app)
{
  var chemiRoutes = require('../controllers/chemicalController');

  app.route('/chemical/getall')
    .post(chemiRoutes.list_chemicals);
  app.route('/chemical/getchemical')
    .post(chemiRoutes.get_chemical);
  app.route('/chemical/create')
    .post(chemiRoutes.create_a_chemical);
  app.route('/chemical/update')
    .post(chemiRoutes.update_a_chemical);
  app.route('/chemical/remove')
    .post(chemiRoutes.delete_a_chemical);
};
