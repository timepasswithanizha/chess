'use strict';
module.exports = function(app)
{
  var fieldRoutes = require('../controllers/fieldController');

  app.route('/field/getall')
    .post(fieldRoutes.list_fields);
  app.route('/field/getfield')
    .post(fieldRoutes.get_field);
  app.route('/field/create')
    .post(fieldRoutes.create_a_field);
  app.route('/field/update')
    .post(fieldRoutes.update_a_field);
  app.route('/field/remove')
    .post(fieldRoutes.delete_a_field);
  app.route('/field/reserve')
    .post(fieldRoutes.reserve_field);

};
