'use strict';
module.exports = function(app)
{
  var weightRoutes = require('../controllers/weightController');


  app.route('/weight/create')
    .post(weightRoutes.create);


};
