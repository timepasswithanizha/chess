'use strict';
module.exports = function(app)
{
  var logRoutes = require('../controllers/logController');

  app.route('/log/getall')
    .post(logRoutes.list_logs);
  app.route('/log/getcompanytime')
    .post(logRoutes.list_logs_by_company_time);
  app.route('/log/create')
    .post(logRoutes.create_a_log);
  app.route('/log/update_reported')
    .post(logRoutes.update_a_log_reported);




};
