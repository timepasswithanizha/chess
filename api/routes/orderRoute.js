'use strict';
module.exports = function(app)
{
  var orderRoutes = require('../controllers/orderController');

  app.route('/order/getall')
    .post(orderRoutes.list_orders);
  app.route('/order/getorder')
    .post(orderRoutes.get_order);
  app.route('/order/create')
    .post(orderRoutes.create_a_order);
  app.route('/order/update')
    .post(orderRoutes.update_a_order);
  app.route('/order/remove')
    .post(orderRoutes.delete_a_order);
  app.route('/order/get_bycompany')
    .post(orderRoutes.get_order_by_company);

};
