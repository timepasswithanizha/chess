'use strict';
module.exports = function (app) {
  const route = require('../controllers/SmartContractApiController');

  app.route('/')
    .get(route.home);

  app.route('/searchOrder')
    .get(route.searchOrder);

  app.route('/searchContainer')
    .get(route.searchContainer);

};
