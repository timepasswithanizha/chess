'use strict';
module.exports = function(app)
{
  var gasJobRoutes = require('../controllers/gasJobController');

  app.route('/gasjob/getall')
    .post(gasJobRoutes.list_all_jobs);
  app.route('/gasjob/create')
    .post(gasJobRoutes.create_a_job);
  app.route('/gasjob/update')
    .post(gasJobRoutes.update);

};
