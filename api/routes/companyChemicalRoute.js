'use strict';
module.exports = function(app)
{
  var compchemRoutes = require('../controllers/companyChemicalController');

  app.route('/compchem/getall')
    .post(compchemRoutes.list_compchems);
  app.route('/compchem/getfield')
    .post(compchemRoutes.get_compchem);
  app.route('/compchem/create')
    .post(compchemRoutes.create_compchems);
  app.route('/compchem/update')
    .post(compchemRoutes.update_compchems);
  app.route('/compchem/remove')
    .post(compchemRoutes.delete_compchems);
};
