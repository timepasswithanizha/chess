
/*
	Vaste palvelualusta websocket services

	Centria Ammattikorkeakoulu Oy


*/
const WebSocket = require('ws');

module.exports = function (wss)
{
	try{
		wss.addEventListener('open',(a) => {
			console.log("ws open");
			//console.log(a);
		});
		wss.addEventListener('close',(b) => {
			console.log("ws close");
			//console.log(b);
		});
		wss.addEventListener('message',(c) => {
			//console.log("message");
			//console.log(c);
		});
		wss.addEventListener('error',(d) => {
			console.log("ws error");
			//console.log(d);
		});
		/*wss.on('connection', function connection(ws) {
			ws.on('error', () => {
				console.log("error");
			});
			ws.on('message', function incoming(message) {
				//console.log('received: %s', message);
				//console.log(wss.clients);
				/*wss.clients.forEach(function each(client) {
					if (client !== ws && client.readyState === WebSocket.OPEN) {
						client.send(message);
					}
				});*/
			/* });
			ws.on('close', function close() {
				  console.log('disconnected');

			});*/

			//ws.send('something');
		//});

	}
	catch (e)
	{
		console.log(e);
		console.log("error");
	}


}


var sendMessage = function (message,wss)
{
	console.log(message);
		if (wss.readyState === WebSocket.OPEN) {
			wss.send(message.toString());
		}
}
module.exports.sendMessage = sendMessage;
