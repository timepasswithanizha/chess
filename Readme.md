
#prerequisites
  1. nodejs
  2. ganache
  3. truffle

#Steps
  1. npm install
  2. Create Folder Named 'encryption'
  3. Create File Inside encryption : 'environment.json'
  eg: environment.json
  {
    "environment":"test",
    "port":45735,
    "providerUrl":"http://127.0.0.1:8545",
    "networkID":"5777",
    "privateKey":"f722c0fef6612bebafca7fd9624592e974c4513bc954ce57fddbcbfef5df973e",
    "publicKey":"0x8AeD87880b22851df5f459E327eE1143566e0A9a"
  }

  4. then : truffle migrate --reset

  5. finelly : npm run start
