// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;

contract ChessBase {
    address public admin;
    constructor() {
        admin = msg.sender;
    }
    struct Order {
        string companyID;
        string contact;
        uint amount;
        uint containers;
        uint _type;
        uint size;
        uint dateStart;
        uint dateEnd;
        uint totalamount;
        uint totalamountunit;
        string chemicalID;
        string suggestion_time;
    }
    struct Container {
        uint dateEnter;
        uint dateLeave;
        uint weight;
        uint volume;
        uint _type;
        uint size;
        uint blockX;
        uint blockY;
        uint status;
        uint calculatedCo2;
    }
    struct ContainerData {
        string containerID_explanation;
        string reservationID;
        string blockCell;
        string containerID;
        string orderID;
        string trackerID;
        string cargoDate;
        string cargoContact;
    }
    mapping (string => Container) public container; 
    mapping (string => ContainerData) public containerData; 
    mapping (string => Order) public orders; 

    event newOrderEvent(string order_id);
    event newContainerEvent(string container_id);
    event containerCo2Event(string container_id, uint co2);
    event deleteOrderEvent(string order_id);
    event deleteContainerEvent(string container_id);
    
    modifier onlyAdmin(){
        require(msg.sender == admin,"only Admin has the access");
        _;
    }
    function writeOrder(
        string calldata orderID,
        string[4] calldata orderData,
        uint[8] calldata orderUnits
        ) public onlyAdmin {
            orders[orderID] = Order(
                orderData[0],
                orderData[1],
                orderUnits[0],
                orderUnits[1],
                orderUnits[2],
                orderUnits[3],
                orderUnits[4],
                orderUnits[5],
                orderUnits[6],
                orderUnits[7],
                orderData[2],
                orderData[3]
            );
        emit newOrderEvent(orderID);
    }   
    
    function writeContainer(
        string calldata containerID,
        string[8] calldata cargoData,
        uint[9] calldata containerUnits
        ) public onlyAdmin {
            container[containerID] = Container(
                containerUnits[0],
                containerUnits[1], 
                containerUnits[2], 
                containerUnits[3], 
                containerUnits[4], 
                containerUnits[5], 
                containerUnits[6],
                containerUnits[7],
                containerUnits[8],0);
            containerData[containerID] = ContainerData(
                cargoData[0],
                cargoData[1],
                cargoData[2],
                cargoData[3],
                cargoData[4],
                cargoData[5],
                cargoData[6], 
                cargoData[7]);
                emit newContainerEvent(containerID);
    }
    function writeCo2(string calldata containerID, uint co2) public onlyAdmin {
        container[containerID].calculatedCo2 = co2;
        emit containerCo2Event(containerID, co2);
    }
    function deleteOrder(string calldata orderID) public onlyAdmin {
        delete orders[orderID];
        emit deleteOrderEvent(orderID);
    }
    function deleteContainer(string calldata containerID) public onlyAdmin {
        delete container[containerID];
        delete containerData[containerID];  
        emit deleteContainerEvent(containerID);
    } 

}   
   

