const path = require('path'),
  Web3 = require('web3'),
  fs = require('fs'),
  environment = JSON.parse(fs.readFileSync("./encryption/environment.json")),
  MyContractJSON = require(path.join(__dirname, 'build/contracts/ChessBase.json')),
  contractAddress = MyContractJSON.networks[environment.networkID].address,
  contractAbi = MyContractJSON.abi,
  web3 = new Web3(new Web3.providers.HttpProvider(environment.providerUrl));
web3.eth.accounts.wallet.add(environment.privateKey);
module.exports = { publicKey: environment.publicKey, CHESS: new web3.eth.Contract(contractAbi, contractAddress) };