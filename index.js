'use strict';
const fs = require('fs');
const http = require('http');
const environmentJson = fs.readFileSync("./encryption/environment.json");
const environment = JSON.parse(environmentJson);

// Constants
const PORT = environment.port;
const HOST = 'localhost';

const express = require('express'),
  path = require('path'),
  app = express(),
  exphbs = require('express-handlebars'),
  mongoose = require('mongoose'),
  bodyParser = require('body-parser'),
  User = require('./api/models/userModel'),
  Chemical = require('./api/models/chemicalModel'),
  Field = require('./api/models/fieldModel'),
  Order = require('./api/models/orderModel'),
  Container = require('./api/models/containerModel'),
  CompanyChemical = require('./api/models/companyChemicalModel'),
  Log = require('./api/models/logModel'),
  Inspection = require('./api/models/inspectionModel'),
  reporter = require('./api/models/reportModel'),
  jobi = require('./api/models/jobModel'),
  gas = require('./api/models/gasModel'),
  gascont = require('./api/models/gasContModel'),
  gasjobs = require('./api/models/gasJobModel');

//Mongoose yhteys
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);
if (environment.environment == 'prod') {
  mongoose.connect('mongodb://localhost/MaterialField');
}
else {
  mongoose.connect('mongodb://localhost/MaterialFieldTest');
}


app.engine('hbs', exphbs({
  defaultLayout: 'main',
  extname: '.hbs'
}));

app.set('view engine', 'hbs');
app.use(express.static(path.join(__dirname, 'public')));


//määritellään bodyparser käyttöön
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const httpServer = http.createServer(app);


// const webs = require('ws');
// const ReconnectingWebSocket = require('reconnecting-websocket');
// //const wss = new WebSocket('ws://127.0.0.1:6894/',"echo-protocol");
// const wss = new ReconnectingWebSocket('ws://127.0.0.1:6894/',["echo-protocol"],{WebSocket:webs,connectionTimeout:5000});
// require('./websocket')(wss);
// const ws = require('./websocket');

// app.use(function(req,res,next){
//   if (req.path == '/container/updatestatus')
//   {
//     const m = JSON.stringify({"type":"imoupdate","reservationID":req.body.reservationID,"status":req.body.status});
//     ws.sendMessage(m,wss);
//     next();
//   }
//   else {
//     next();
//   }
// });





const userRoute = require('./api/routes/userRoute');
userRoute(app);
const chemicalRoute = require('./api/routes/chemicalRoute');
chemicalRoute(app);
const fieldRoute = require('./api/routes/fieldRoute');
fieldRoute(app);
const orderRoute = require('./api/routes/orderRoute');
orderRoute(app);
const containerRoute = require('./api/routes/containerRoute');
containerRoute(app);
const companyChemicalRoute = require('./api/routes/companyChemicalRoute');
companyChemicalRoute(app);
const logRoute = require('./api/routes/logRoute');
logRoute(app);
const inspectionRoute = require('./api/routes/inspectionRoute');
inspectionRoute(app);
const reportRoute = require('./api/routes/reportRoute');
reportRoute(app);
const jobRoute = require('./api/routes/jobRoute');
jobRoute(app);
const gasRoute = require('./api/routes/gasRoute');
gasRoute(app);
const gasContRoute = require('./api/routes/gasContRoute');
gasContRoute(app);
const gasJobRoute = require('./api/routes/gasJobRoute');
gasJobRoute(app);
const SmartContractApiRoute = require('./api/routes/smartContractApiRoute');
SmartContractApiRoute(app);

httpServer.listen(PORT);
console.log(`Running on http://${HOST}:${PORT}`);
